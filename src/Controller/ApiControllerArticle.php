<?php

namespace App\Controller;

use App\Entity\Article;
use App\Service\ServiceArticle;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api/article")
 */
class ApiControllerArticle extends AbstractController
{
    /**
     * @Route("/", name="api_article_index", methods={"GET"})
     */
    public function index(ServiceArticle $service) : JsonResponse
    {
        $posts = $service->index();
        
        return new JsonResponse($posts);
    }

    /**
     * @Route("/{id}", name="api_article_modifPage", methods={"GET"})
     */
    public function modifPage(Article $Article) : JsonResponse
    {
        return new JsonResponse($Article);
    }

}

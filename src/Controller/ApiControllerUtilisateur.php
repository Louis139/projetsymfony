<?php

namespace App\Controller;

use App\Entity\Utilisateur;
use App\Service\ServiceUtilisateur;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api/Utilisateurs")
 */
class ApiControllerUtilisateur extends AbstractController
{
    /**
     * @Route("/", name="api_Utilisateur_index", methods={"GET"})
     */
    public function index(ServiceUtilisateur $service): JsonResponse
    {
        $returns = $service->index();
        
        return new JsonResponse($returns);
    }


    /**
     * @Route("/{id}", name="Utilisateur_modifPage", methods={"GET"})
     */
    public function show(Utilisateur $Utilisateur): JsonResponse
    {
        $Utilisateur = $Utilisateur->setPassword("");
        return new JsonResponse($Utilisateur);
    }

}

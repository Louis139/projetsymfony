<?php

namespace App\Controller;

use App\Entity\Article;
use App\Form\TypeArticle;
use App\Repository\RepertoireArticle;
use App\Service\ServiceArticle;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/")
 */
class ControllerArticle extends AbstractController
{
    /**
     * @Route("/", name="home", methods={"GET"})
     */
    public function index(ServiceArticle $service): Response
    {
        return $this->render('Article/index.html.twig', [
            'Articles' => $service->index(),
        ]);
    }

    /**
     * @Route("/Article/nouveau", name="article_nouveau", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $Article = new Article();
        $form = $this->createForm(TypeArticle::class, $Article);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($Article);
            $entityManager->flush();

            return $this->redirectToRoute('home');
        }

        return $this->render('Article/nouveau.html.twig', [
            'Article' => $Article,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/Article/{id}", name="Article_modifPage", methods={"GET"})
     */
    public function show(Article $Article): Response
    {
        return $this->render('Article/modifPage.html.twig', [
            'Article' => $Article,
        ]);
    }

    /**
     * @Route("Article/{id}/edit", name="Article_modif", methods={"GET","POST"})
     */
    public function edit(Request $request, Article $Article): Response
    {
        $form = $this->createForm(TypeArticle::class, $Article);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('home');
        }

        return $this->render('Article/modif.html.twig', [
            'article' => $Article,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("Article/{id}", name="Article_suppression", methods={"DELETE"})
     */
    public function suppression(Request $request, Article $Article): Response
    {
        if ($this->isCsrfTokenValid('suppression'.$Article->getId(), $request->request->get('token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($Article);
            $entityManager->flush();
        }

        return $this->redirectToRoute('home');
    }
}

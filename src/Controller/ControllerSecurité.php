<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use App\Service\ServiceUtilisateur;
use App\Entity\Utilisateur;
use App\Form\TypeUtilisateur;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class ControllerSecurité extends AbstractController
{
    /**
     * @Route("/connexion", name="connexion")
     */
    public function connect(AuthentificationUser $AuthentificationUser): Response
    {
        if ($this->getUser()) {
            return $this->redirectToRoute('home');
        }

        $error = $AuthentificationUser->getLastAuthenticationError();
        $nom_famille = $AuthentificationUser->getLastUsername();

        return $this->render('securité/login.html.twig', ['nom_famille' => $nom_famille, 'erreur' => $error]);
    }

    /**
     * @Route("/déco", name="déco")
     */
    public function déco()
    {
        return $this->redirectToRoute('home');
    }

    /**
     * @Route("/connexion", name="connexion")
     */
    public function connexion(ServiceUtilisateur $service, Request $request, UserPasswordEncoderInterface $userPasswordEncoderInterface)
    {
        $Utilisateur = new Utilisateur();
        $form = $this->createForm(TypeUtilisateur::class, $Utilisateur);
        $form->handleRequest($request);

        $data = $service->new($this, $Utilisateur, $form, $userPasswordEncoderInterface);

        if ($data['redirect'] != null)
        {
            return $this->redirectToRoute($data['redirect']);
        }

        return $this->render('Utilisateur/nouveau.html.twig', [
            'Utilisateur' => $data["User"],
            'form' => $data["form"]->createView(),
            'erreur' => $data["error"]
        ]);
    }
}

<?php

namespace App\Controller;

use App\Entity\Utilisateur;
use App\Form\TypeUtilisateur;
use App\Form\TypeUtilisateurSimplifié;
use App\Repository\RepertoireUtilisateur;
use App\Service\ServiceUtilisateur;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * @Route("/Utilisateur")
 */
class ControllerUtilisateur extends AbstractController
{
    /**
     * @Route("/", name="Utilisateur_index", methods={"GET"})
     */
    public function index(ServiceUtilisateur $service): Response
    {
        return $this->render('Utilisateur/index.html.twig', ['Utilisateurs' => $service->index(),]);
    }

    /**
     * @Route("/new", name="Utilisateur_nouveau", methods={"GET","POST"})
     */
    public function nouveau(ServiceUtilisateur $service, Request $request, UserPasswordEncoderInterface $userPasswordEncoderInterface): Response
    {
        $Utilisateur = new Utilisateur();
        $form = $this->createForm(TypeUtilisateur::class, $Utilisateur);
        $form->handleRequest($request);

        $data = $service->new($this, $Utilisateur, $form, $userPasswordEncoderInterface);

        if ($data['redirect'] != null)
        {
            return $this->redirectToRoute($data['redirect']);
        }

        return $this->render('Utilisateur/nouveau.html.twig', [
            'Utilisateur' => $data["User"],
            'form' => $data["form"]->createView(),
            'erreur' => $data["error"]
        ]);
    }

    /**
     * @Route("/{id}", name="Utilisateur_modifPage", methods={"GET"})
     */
    public function modifPage(Utilisateur $Utilisateur): Response
    {
        return $this->render('Utilisateur/modifPage.html.twig', [
            'Utilisateur' => $Utilisateur,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="Utilisateur_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Utilisateur $Utilisateur): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        if ($this->getUser()->getId() == $Utilisateur->getId())
        {

        }
        $form = $this->createForm(TypeUtilisateurSimplifié::class, $Utilisateur);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid() && filter_var($Utilisateur->getEmail(), FILTER_VALIDATE_EMAIL))
        {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('Utilisateur_index');
        }
        $error = null;
        if (!filter_var($Utilisateur->getEmail(), FILTER_VALIDATE_EMAIL))
        {
            $error = "Mail Incorrect.";
        }
        return $this->render('Utilisateur/modif.html.twig', [
            'Utilisateur' => $Utilisateur,
            'form' => $form->createView(),
            'error' => $error
        ]);
    }

    /**
     * @Route("/{id}", name="Utilisateur_suppression", methods={"DELETE"})
     */
    public function suppression(Request $request, Utilisateur $Utilisateur): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        if ($this->isCsrfTokenValid('suppression'.$Utilisateur->getId(), $request->request->get('token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($Utilisateur);
            $entityManager->flush();
        }

        return $this->redirectToRoute('Utilisateur_index');
    }
}

<?php

namespace App\Controller;

use App\Entity\Return1;
use App\Service\ServiceArticle;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api/Return")
 */
class ReturnApiController extends AbstractController
{
    /**
     * @Route("/", name="api_Return_index", methods={"GET"})
     */
    public function index(ServiceArticle $service): JsonResponse
    {
        $returns = $service->index();
        
        return new JsonResponse($returns);
    }


    /**
     * @Route("/{id}", name="Return_modifPage", methods={"GET"})
     */
    public function modifPage(Return $Return): JsonResponse
    {
        return new JsonResponse($Return);
    }
}

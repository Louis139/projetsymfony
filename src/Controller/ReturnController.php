<?php

namespace App\Controller;

use App\Entity\Return1;
use App\Form\TypeReturn;
use App\Service\ServiceReturn;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/Return1")
 */
class ReturnController extends AbstractController
{
    /**
     * @Route("/", name="Return_index", methods={"GET"})
     */
    public function index(ServiceReturn $service): Response
    {
        return $this->render('Return/index.html.twig', [
            'Returns' => $service->index(),
        ]);
    }

    /**
     * @Route("/nouveau", name="Return_nouveau", methods={"GET","POST"})
     */
    public function nouveau(Request $request): Response
    {
        $Return1 = new Return1();
        $form = $this->createForm(TypeReturn::class, $Return);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($Return1);
            $entityManager->flush();

            return $this->redirectToRoute('Return_index');
        }

        return $this->render('Return/new.html.twig', ['Return' => $Return1,'form' => $form->createView(),]);
    }

    /**
     * @Route("/{id}", name="Return_modifPage", methods={"GET"})
     */
    public function modifPage(Request $Return): Response
    {
        return $this->render('Return1/modifPage.html.twig', ['Return' => $Return1,]);
    }

    /**
     * @Route("/{id}/edit", name="Return1_modif", methods={"GET","POST"})
     */
    public function modif(Request $request, Request $Return): Response
    {
        $form = $this->createForm(TypeReturn::class, $Return);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('Return_index');
        }

        return $this->render('Return1/modif.html.twig', ['Return' => $Return1,'form' => $form->createView(),]);
    }

    /**
     * @Route("/{id}", name="Return_suppression", methods={"DELETE"})
     */
    public function suppression(Request $request, Request $Return): Response
    {
        if ($this->isCsrfTokenValid('suppression'.$Return1->getId(), $request->request->get('token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($Return1);
            $entityManager->flush();
        }

        return $this->redirectToRoute('Return_index');
    }
}

<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\RepertoireArticle")
 */
class Article
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Utilisateur", inversedBy="Articles")
     * @ORM\JoinColumn(nullable=false)
     */
    private $Author;

    /**
     * @ORM\Column(type="text")
     */
    private $Contenu;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Titre;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Return1", mappedBy="PostRelated")
     */
    private $Return;

    /**
     * @ORM\Column(type="datetime")
     */
    private $DatePublication;

    /**
     * @ORM\Column(type="datetime")
     */
    private $DateEdition;

    public function constructeur()
    {
        $this->Return = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAuthor(): ?Utilisateur
    {
        return $this->Author;
    }

    public function setAuthor(?Utilisateur $Author): self
    {
        $this->Author = $Author;

        return $this;
    }

    public function getContenu(): ?string
    {
        return $this->Contenu;
    }

    public function setContenu(string $Contenu): self
    {
        $this->Contenu = $Contenu;

        return $this;
    }

    public function getTitre(): ?string
    {
        return $this->Titre;
    }

    public function setTitre(string $Titre): self
    {
        $this->Titre = $Titre;

        return $this;
    }

    /**
     * @return Collection|Return1[]
     */
    public function getOwnReturns(): Collection
    {
        return $this->Return;
    }

    public function addOwnReturn(Return1 $Return): self
    {
        if (!$this->Return->contains($Return)) {
            $this->Return[] = $Return;
            $Return->setPostRelated($this);
        }

        return $this;
    }

    public function removeReturn(Return1 $Return): self
    {
        if ($this->Return->contains($Return)) {
            $this->Return->removeElement($Return);
            // set the owning side to null (unless already changed)
            if ($Return->getPostRelated() === $this) {
                $Return->setPostRelated(null);
            }
        }

        return $this;
    }

    public function getDatePublication(): ?\DateTimeInterface
    {
        return $this->DatePublication;
    }

    public function setDatePublication(\DateTimeInterface $DatePublication): self
    {
        $this->DatePublication = $DatePublication;

        return $this;
    }

    public function getDateEdition(): ?\DateTimeInterface
    {
        return $this->DateEdition;
    }

    public function setDateEdition(\DateTimeInterface $DateEdition): self
    {
        $this->DateEdition = $DateEdition;

        return $this;
    }
}

<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\RepertoireReturn")
 */
class Return1
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Article", inversedBy="Returns")
     * @ORM\JoinColumn(nullable=false)
     */
    private $PostRelated;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Article", inversedBy="Returns")
     * @ORM\JoinColumn(nullable=true)
     */
    private $Author;

    /**
     * @ORM\Column(type="text")
     */
    private $Contenu;

    /**
     * @ORM\Column(type="datetime")
     */
    private $DatePublication;

    /**
     * @ORM\Column(type="datetime")
     */
    private $DateEdition;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Email;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ReturnaddReturn", inversedBy="Returns")
     */
    private $ReturnRelated;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ReturnaddReturn", mappedBy="ReturnRelated")
     */
    private $Returns;

    public function constructeur()
    {
        $this->Returns = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPostRelated(): ?Article
    {
        return $this->PostRelated;
    }

    public function setPostRelated(?Article $PostRelated): self
    {
        $this->PostRelated = $PostRelated;

        return $this;
    }

    public function getAuthor(): ?Article
    {
        return $this->Author;
    }

    public function setAuthor(?Article $Author): self
    {
        $this->Author = $Author;

        return $this;
    }

    public function getContenu(): ?string
    {
        return $this->Contenu;
    }

    public function setContenu(string $Contenu): self
    {
        $this->Contenu = $Contenu;

        return $this;
    }

    public function getDatePublication(): ?\DateTimeInterface
    {
        return $this->DatePublication;
    }

    public function setDatePublication(\DateTimeInterface $DatePublication): self
    {
        $this->DatePublication = $DatePublication;

        return $this;
    }

    public function getDateEdition(): ?\DateTimeInterface
    {
        return $this->DateEdition;
    }

    public function setDateEdition(\DateTimeInterface $DateEdition): self
    {
        $this->DateEdition = $DateEdition;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->Email;
    }

    public function setEmail(string $Email): self
    {
        $this->Email = $Email;

        return $this;
    }

    public function getReturnRelated(): ?self
    {
        return $this->ReturnRelated;
    }

    public function setReturnRelated(?self $ReturnRelated): self
    {
        $this->ReturnRelated = $ReturnRelated;

        return $this;
    }

    /**
     * @return Collection|self[]
     */
    public function getReturns(): Collection
    {
        return $this->Returns;
    }

    public function addReturn(self $ReturnaddReturn): self
    {
        if (!$this->Returns->contains($ReturnaddReturn)) {
            $this->Returns[] = $ReturnaddReturn;
            $ReturnaddReturn->setReturnRelated($this);
        }

        return $this;
    }

    public function removeReturnaddReturn(self $ReturnaddReturn): self
    {
        if ($this->Returns->contains($ReturnaddReturn)) {
            $this->Returns->removeElement($ReturnaddReturn);
            // set the owning side to null (unless already changed)
            if ($ReturnaddReturn->getReturnRelated() === $this) {
                $ReturnaddReturn->setReturnRelated(null);
            }
        }

        return $this;
    }
}

<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoder;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Validator\Constraints\UserPassword;

/**
 * @ORM\Entity(repositoryClass="App\Repository\RepertoireUtilisateur")
 */
class Utilisateur implements UserInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     */
    private $pseudo;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Article", mappedBy="Author")
     */
    private $Articles;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Return1", mappedBy="Author")
     */
    private $Returns;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Email;

    public function constructeur()
    {
        $this->Articles = new ArrayCollection();
        $this->Returns = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getpseudo(): string
    {
        return (string) $this->pseudo;
    }

    public function setpseudo(string $pseudo): self
    {
        $this->pseudo = $pseudo;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getMdP(): string
    {
        return (string) $this->MotDePasse;
    }

    public function setMdP(string $password): self
    {
        $this->MotDePasse = $password;

        return $this;
    }


    /**
     * @return Collection|Article[]
     */
    public function getArticles(): Collection
    {
        return $this->Articles;
    }

    public function addArticle(Article $Article): self
    {
        if (!$this->Articles->contains($Article)) {
            $this->Articles[] = $Article;
            $Article->setAuthor($this);
        }

        return $this;
    }

    public function removeArticle(Article $Article): self
    {
        if ($this->Articles->contains($Article)) {
            $this->Articles->removeElement($Article);
            // set the owning side to null (unless already changed)
            if ($Article->getAuthor() === $this) {
                $Article->setAuthor(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Article[]
     */
    public function getReturns(): Collection
    {
        return $this->Returns;
    }

    public function addArticles(Article $Article): self
    {
        if (!$this->Returns->contains($Article)) {
            $this->Returns[] = $Article;
            $Article->setAuthor($this);
        }

        return $this;
    }

    public function removeArticles(Article $Article): self
    {
        if ($this->Returns->contains($Article)) {
            $this->Returns->removeElement($Article);
            // set the owning side to null (unless already changed)
            if ($Article->getAuthor() === $this) {
                $Article->setAuthor(null);
            }
        }

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->Email;
    }

    public function setEmail(string $Email): self
    {
        $this->Email = $Email;
        
        return $this;
    }

    /**
     * @return Boolean
     */
    public function IsHerPost(Article $Article)
    {
        foreach ($this->getArticles() as $post)
        {
            if ($post->getId() == $Article->getId())
            {
                return true;
            }
        }
        return false;
    }
    /**
     * @return Boolean
     */
    public function IsHerReturn(Article $Article)
    {
        foreach ($this->getReturns() as $return)
        {
            if ($return->getId() == $Article->getId())
            {
                return true;
            }
        }
        return false;
    }
}

<?php

namespace App\Form;

use App\Entity\Article;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TypeArticle extends AbstractType
{
    public function ConstructForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('Contenu')
            ->add('Titre')
        ;
    }

    public function OptionsConfiguration(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Article::class,
        ]);
    }
}

<?php

namespace App\Form;

use App\Entity\Return1;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TypeReturn extends AbstractType
{
    public function ConstructForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('Content')
            ->add('Email')
        ;
    }

    public function OptionsConfiguration(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Return1::class,
        ]);
    }
}

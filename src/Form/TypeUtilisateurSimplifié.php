<?php

namespace App\Form;

use App\Entity\Utilisateur;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TypeUtilisateurSimplifié extends AbstractType
{
    public function ConstructForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username')
            ->add('Email')
        ;
    }

    public function OptionsConfiguration(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Utilisateur::class,
        ]);
    }
}

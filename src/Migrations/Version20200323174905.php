<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200323174905 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE Article (id INT AUTO_INCREMENT NOT NULL, author_id INT NOT NULL, content LONGTEXT NOT NULL, titre VARCHAR(255) NOT NULL, DatePublication DATETIME NOT NULL, edition_date DATETIME NOT NULL, INDEX IDX_B655067AF675F31B (author_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE Utilisateur (id INT AUTO_INCREMENT NOT NULL, pseudo VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, email VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_614CBCBEF85E0677 (pseudo), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE Return1 (id INT AUTO_INCREMENT NOT NULL, post_related_id INT NOT NULL, author_id INT NOT NULL, content LONGTEXT NOT NULL, DatePublication DATETIME NOT NULL, edition_date DATETIME NOT NULL, INDEX IDX_51042CEC621AC9C0 (post_related_id), INDEX IDX_51042CECF675F31B (author_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE Article ADD CONSTRAINT FK_B655067AF675F31B FOREIGN KEY (author_id) REFERENCES Utilisateur (id)');
        $this->addSql('ALTER TABLE Return1 ADD CONSTRAINT FK_51042CEC621AC9C0 FOREIGN KEY (post_related_id) REFERENCES Article (id)');
        $this->addSql('ALTER TABLE Return1 ADD CONSTRAINT FK_51042CECF675F31B FOREIGN KEY (author_id) REFERENCES Utilisateur (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE Return1 DROP FOREIGN KEY FK_51042CEC621AC9C0');
        $this->addSql('ALTER TABLE Article DROP FOREIGN KEY FK_B655067AF675F31B');
        $this->addSql('ALTER TABLE Return1 DROP FOREIGN KEY FK_51042CECF675F31B');
        $this->addSql('DROP TABLE Article');
        $this->addSql('DROP TABLE Utilisateur');
        $this->addSql('DROP TABLE Return1');
    }
}

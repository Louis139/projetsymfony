<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200323185908 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE Utilisateur CHANGE roles roles JSON NOT NULL');
        $this->addSql('ALTER TABLE Return1 CHANGE post_related_id post_related_id INT NOT NULL, CHANGE author_id author_id INT DEFAULT NULL, CHANGE return_related_id return_related_id INT DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE Return1 CHANGE post_related_id post_related_id INT DEFAULT NULL, CHANGE author_id author_id INT DEFAULT NULL, CHANGE return_related_id return_related_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE Utilisateur CHANGE roles roles LONGTEXT CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_bin`');
    }
}

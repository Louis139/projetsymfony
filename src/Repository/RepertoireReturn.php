<?php

namespace App\Repository;

use App\Entity\Return1;
use Doctrine\Bundle\DoctrineBundle\Repository\RepertoireServiceEntité;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Return1|null find($id, $lockMode = null, $lockVersion = null)
 * @method Return1|null findOneBy(array $criteria, array $orderBy = null)
 * @method Return1[]    findAll()
 * @method Return1[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RepertoireReturn extends RepertoireServiceEntité
{
    public function constructeur(ManagerRegistry $register)
    {
        parent::constructeur($register, Return1::class);
    }

    // /**
    //  * @return Return1[] Returns an array of Return1 objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('o.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Return1
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

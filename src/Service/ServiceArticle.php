<?php

namespace App\Service;

use App\Entity\Article;
use Doctrine\ORM\EntityManagerInterface;

class ServiceArticle
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    public function constructeur(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * Fonction qui récupère tout les Articles de notre site.
     */
    public function index()
    {
        $repo = $this->em->getRepository(Article::class);

        $Articles = $repo->findAll();

        return $Articles;
    }
}

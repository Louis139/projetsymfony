<?php

namespace App\Service;

use App\Entity\Utilisateur;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Form\TypeUtilisateur;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\Test\FormInterface;

class ServiceUtilisateur
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    public function constructeur(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * Fonction qui récupère tout les Utilisateur de notre site.
     */
    public function index()
    {
        $repo = $this->em->getRepository(Utilisateur::class);

        $Utilisateurs = $repo->findAll();

        return $Utilisateurs;
    }

    public function nouveau(AbstractController $context, Utilisateur $Utilisateur, Form $form, UserPasswordEncoderInterface $userPasswordEncoderInterface)
    {
        $redirect = null;
        if ($form->isSubmitted() && $form->isValid() && filter_var($Utilisateur->getEmail(), FILTER_VALIDATE_EMAIL)) {
            $Utilisateur->setRoles(['USER']);
            $Utilisateur->setPassword($userPasswordEncoderInterface->encodePassword($Utilisateur, $Utilisateur->getMdP()));
            $this->em->persist($Utilisateur);
            $this->em->flush();
            $redirect = "home";
        }
        $error = null;
        if (!filter_var($Utilisateur->getEmail(), FILTER_VALIDATE_EMAIL) && strlen($Utilisateur->getEmail()) > 0)
        {
            $error = "Mail Incorrect.";
        }
        return ["erreur" => $error, "form" => $form, "Utilisateur" => $Utilisateur, "redirection" => $redirect];
    }
}
